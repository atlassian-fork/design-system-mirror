import React from 'react';
import Avatar from '../src';

const AvatarCircle = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Avatar name="xxlarge" size="xxlarge" appearance="circle" />
    <Avatar name="xlarge" size="xlarge" presence="online" appearance="circle" />
    <Avatar name="large" size="large" presence="offline" appearance="circle" />
    <Avatar name="medium" size="medium" presence="busy" appearance="circle" />
    <Avatar name="small" size="small" presence="focus" appearance="circle" />
    <Avatar name="xsmall" size="xsmall" appearance="circle" />
  </div>
);

export default AvatarCircle;

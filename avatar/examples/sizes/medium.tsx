import React from 'react';
import Avatar from '../../src';

const Medium = () => (
  <div>
    <Avatar name="medium" size="medium" />
  </div>
);

export default Medium;

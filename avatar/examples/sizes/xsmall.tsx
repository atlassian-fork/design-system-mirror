import React from 'react';
import Avatar from '../../src';

const XSmall = () => (
  <div>
    <Avatar name="xsmall" size="xsmall" />
  </div>
);

export default XSmall;

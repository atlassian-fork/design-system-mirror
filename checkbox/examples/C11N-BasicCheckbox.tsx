import React from 'react';
import { Checkbox } from '../src';

const BasicCheckbox = () => {
  return (
    <Checkbox
      value="Basic checkbox"
      label="Basic checkbox"
      onChange={() => {}}
      name="checkbox-basic"
      testId="cb-basic"
    />
  );
};

export default BasicCheckbox;

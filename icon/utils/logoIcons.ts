/*
Unlike its sister icon info files, this one is manually curated
*/

import {
    AtlassianIcon,
    BitbucketIcon,
    ConfluenceIcon,
    HipchatIcon,
    JiraCoreIcon,
    JiraServiceDeskIcon,
    JiraSoftwareIcon,
    JiraIcon,
    StatuspageIcon,
    StrideIcon,
  } from '@atlaskit/logo';

  export default {
    AtlassianIcon: {
      componentName: 'AtlassianIcon',
      component: AtlassianIcon,
      keywords: ['product', 'logo', 'atlassian'],
      package: '@atlaskit/logo/AtlassianLogo/Icon'
    },
    BitbucketIcon: {
      componentName: 'BitbucketIcon',
      component: BitbucketIcon,
      keywords: ['product', 'logo', 'bitbucket'],
      package: '@atlaskit/logo/BitbucketLogo/Icon'
    },
    ConfluenceIcon: {
      componentName: 'ConfluenceIcon',
      component: ConfluenceIcon,
      keywords: ['product', 'logo', 'confluence'],
      package: '@atlaskit/logo/ConfluenceLogo/Icon'
    },
    HipchatIcon: {
      componentName: 'HipchatIcon',
      component: HipchatIcon,
      keywords: ['product', 'logo', 'hipchat'],
      package: '@atlaskit/logo/HipchatLogo/Icon'
    },
    JiraCoreIcon: {
      componentName: 'JiraCoreIcon',
      component: JiraCoreIcon,
      keywords: ['product', 'logo', 'jira', 'core'],
      package: '@atlaskit/logo/JiraCoreLogo/Icon'
    },
    JiraServiceDeskIcon: {
      componentName: 'JiraServiceDeskIcon',
      component: JiraServiceDeskIcon,
      keywords: ['product', 'logo', 'jira', 'servicedesk'],
      package: '@atlaskit/logo/JiraServiceDeskLogo/Icon'
    },
    JiraSoftwareIcon: {
      componentName: 'JiraSoftwareIcon',
      component: JiraSoftwareIcon,
      keywords: ['product', 'logo', 'jira', 'software'],
      package: '@atlaskit/logo/JiraSoftwareLogo/Icon'
    },
    JiraIcon: {
      componentName: 'JiraIcon',
      component: JiraIcon,
      keywords: ['product', 'logo', 'jira'],
      package: '@atlaskit/logo/JiraLogo/Icon'
    },
    StatuspageIcon: {
      componentName: 'StatuspageIcon',
      component: StatuspageIcon,
      keywords: ['product', 'logo', 'statuspage'],
      package: '@atlaskit/logo/StatuspageLogo/Icon'
    },
    StrideIcon: {
      componentName: 'StrideIcon',
      component: StrideIcon,
      keywords: ['product', 'logo', 'stride'],
      package: '@atlaskit/logo/StrideLogo/Icon'
    },
  };

import React from 'react';
import InlineMessage from '../../src';

export default function Basic() {
  return (
    <InlineMessage title="Title" secondaryText="secondary text">
      <p>
        I can contain <strong>whatever</strong> ✌️
      </p>
    </InlineMessage>
  );
}

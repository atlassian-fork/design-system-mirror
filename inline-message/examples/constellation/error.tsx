import React from 'react';
import InlineMessage from '../../src';

export default function ErrorDemo() {
  return (
    <InlineMessage type="error">
      <p>
        This name is already in use.
        <br />
        Try another.
      </p>
    </InlineMessage>
  );
}

import React from 'react';
import InlineMessage from '../../src';

export default function Success() {
  return (
    <InlineMessage title="Success" type="confirmation">
      <p>You really did it!</p>
    </InlineMessage>
  );
}

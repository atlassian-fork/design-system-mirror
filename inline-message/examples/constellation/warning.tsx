import React from 'react';
import InlineMessage from '../../src';

export default function Warning() {
  return (
    <InlineMessage
      type="warning"
      secondaryText="New users will be sent a request to join"
    >
      <h4>Multiple accounts</h4>
      <p>
        We will automatically invite any new users to Bitbucket, depending on
        your account settings
      </p>
      <p>
        <a href="#">Update your settings</a> or <a href="#">read more</a>
      </p>
    </InlineMessage>
  );
}

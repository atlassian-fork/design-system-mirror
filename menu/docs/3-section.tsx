import React from 'react';
import { code, md, Props, Example } from '@atlaskit/docs';

export default md`
  Used to group related items in a menu.
  Generally you'll want to **always group items in sections**,
  even if there is only one item in the group.

  ${code`highlight=1,4,7
import { Section } from '@atlaskit/menu';

<MenuGroup>
  <Section>
    <HeadingItem>Actions</HeadingItem>
    <ButtonItem>Create article</ButtonItem>
  </Section>
</MenuGroup>
  `}

  ## Scrollable sections

  While you can create anything you want with menu we don't really ecourage doing _anything_.
  When creating pop-up menus you shouldn't allow it to get too long,
  but you still might have a lot of content to show.

  For that you can use _scrollable_ sections to keep the height at a reasonable length,
  but enable you to show a lot of content.

  By setting \`maxHeight\` to an appropriate height on the \`MenuGroup\` component,
  and then setting \`isScrollable\` on the desired \`Section\` -
  you too can have a scrollable section:

  ### Gotchas

  Scrollable sections will only work as a direct descendant of a menu group.
  This means scrollable **sections in sections will not work**.

  ${(
    <Example
      title="Scrollable sections"
      Component={require('../examples/scrollable-sections.tsx').default}
      source={require('!!raw-loader!../examples/scrollable-sections.tsx')}
    />
  )}

  ${(
    <Props
      heading="Props"
      props={require('!!extract-react-types-loader!../src/components/section/section.tsx')}
    />
  )}
`;

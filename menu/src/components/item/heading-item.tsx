/** @jsx jsx */
import { jsx, CSSObject } from '@emotion/core';

import { itemHeadingCSS } from './styles';
import { HeadingItemProps } from '../types';

const HeadingItem = ({
  children,
  testId,
  cssFn = (currentStyles: CSSObject) => currentStyles,
}: HeadingItemProps) => (
  <div css={cssFn(itemHeadingCSS)} data-testid={testId}>
    {children}
  </div>
);

export default HeadingItem;

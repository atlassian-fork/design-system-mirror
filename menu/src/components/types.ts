import { CSSObject } from '@emotion/core';

export interface MenuGroupSizing {
  /**
   * Useful to constrain the menu group minimum height to a specific value.
   */
  minHeight?: number | string;

  /**
   * Useful to constrain the menu groups height to a specific value.
   * Needs to be set when wanting to have scrollable sections.
   */
  maxHeight?: number | string;

  /**
   * Useful to constrain the menu group minimum width to a specific value.
   */
  minWidth?: number | string;

  /**
   * Useful to constrain the menu group width to a specific value.
   */
  maxWidth?: number | string;
}

export interface MenuGroupProps extends MenuGroupSizing {
  /**
   * Children of the menu group,
   * should generally be `Section` components.
   */
  children: React.ReactNode;

  /**
   * A `testId` prop is provided for specified elements,
   * which is a unique string that appears as a data attribute `data-testid` in the rendered code,
   * serving as a hook for automated tests.
   */
  testId?: string;

  /**
   * Handler called when clicking on this element,
   * or any children elements.
   * Useful when needing to stop propagation of child events.
   */
  onClick?: (event: React.MouseEvent | React.KeyboardEvent) => void;
}

export interface SectionProps {
  /**
   * Enables scrolling within the section.
   * Make sure to set `maxHeight` on the parent `MenuGroup` component else it will not work.
   */
  isScrollable?: boolean;

  /**
   * Will render a border at the top of the section.
   */
  hasSeparator?: boolean;

  /**
   * Children of the section,
   * should generally be `Item` or `Heading` components,
   * but can also be [`EmptyState`](https://atlaskit.atlassian.com/packages/core/empty-state)s when wanting to render errors.
   */
  children: React.ReactNode;

  /**
   * A `testId` prop is provided for specified elements,
   * which is a unique string that appears as a data attribute `data-testid` in the rendered code,
   * serving as a hook for automated tests.
   */
  testId?: string;
}

export interface BaseItemProps {
  /**
   * A function that can be used to override the styles of
   * the LinkItem. It receives the current styles and state
   * and expects a styles object.
   */
  cssFn?: CSSFn;

  /**
   * Element to render before the item text.
   * Generally should be an [`Icon`](https://atlaskit.atlassian.com/packages/core/icon) component.
   */
  elemBefore?: React.ReactNode;

  /**
   * Element to render after the item text.
   * Generally should be an [`Icon`](https://atlaskit.atlassian.com/packages/core/icon) component.
   */
  elemAfter?: React.ReactNode;

  /**
   * Event that is triggered when the element is clicked.
   */
  onClick?: (event: React.MouseEvent | React.KeyboardEvent) => void;

  /**
   * Description of the item.
   * This will render smaller text below the primary text of the item as well as slightly increasing the height of the item.
   */
  description?: string | JSX.Element;

  /**
   * Makes the element appear disabled as well as removing interactivity.
   */
  isDisabled?: boolean;

  /**
   * Makes the element appear selected.
   */
  isSelected?: boolean;

  /**
   * Primary content for the item.
   */
  children?: React.ReactNode;

  /**
   * A `testId` prop is provided for specified elements,
   * which is a unique string that appears as a data attribute `data-testid` in the rendered code,
   * serving as a hook for automated tests.
   */
  testId?: string;
}

export interface ButtonItemProps extends BaseItemProps {}

export interface LinkItemProps extends BaseItemProps {
  /**
   * Link to another page.
   */
  href?: string;

  /**
   * Where to display the linked URL,
   * see [anchor information](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a) on mdn for more information.
   */
  target?: string;

  /**
   * The relationship of the linked URL as space-separated link types.
   * Generally you'll want to set this to "noopener noreferrer" when `target` is "_blank".
   */
  rel?: string;
}

export interface CustomItemComponentProps {
  /**
   * The children of the item.
   */
  children: React.ReactNode;

  /**
   * Class to apply to the root container of the custom component,
   * ensure this has been applied so the consistent item styling is applied.
   */
  wrapperClass: string;

  /**
   * Test id that is passed through to the custom component.
   */
  'data-testid'?: string;

  /**
   * Event handler that is passed through to the custom component.
   */
  onClick?: (event: React.MouseEvent | React.KeyboardEvent) => void;
}

export interface CustomItemProps extends BaseItemProps {
  /**
   * Custom component to render as an item.
   * This can be both a functional component or a class component.
   * **Will return `null` if no component is defined.**

   * **NOTE:** Make sure the reference for this component does not change between renders else undefined behavior may happen.
   */
  component?: React.ComponentType<CustomItemComponentProps>;
}

export interface SkeletonItemProps {
  /**
   * Renders a skeleton circle in the `elemBefore` location.
   * Takes priority over `hasIcon.
   */
  hasAvatar?: boolean;

  /**
   * Renders a skeleton square in the `elemBefore` location.
   */
  hasIcon?: boolean;

  /**
   * Width of the skeleton item.
   * Generally you don't need to specify this as it has a staggered width based on `:nth-child` by default.
   */
  width?: string | number;

  /**
   * A `testId` prop is provided for specified elements,
   * which is a unique string that appears as a data attribute `data-testid` in the rendered code,
   * serving as a hook for automated tests.
   */
  testId?: string;

  /**
   * Causes to the skeleton to have a slight horizontal shimmer.
   * Only use this when you want to bring more attention to the loading content.
   */
  isShimmering?: boolean;
}

export type Width = string | number;

export interface HeadingItemProps {
  /**
   * A function that can be used to override the styles of
   * the HeadingItem. It receives the current styles and returns
   * a customised styles object.
   */
  cssFn?: StatelessCssFn;

  /**
   * The text of the heading.
   */
  children: React.ReactNode;

  /**
   * A `testId` prop is provided for specified elements,
   * which is a unique string that appears as a data attribute `data-testid` in the rendered code,
   * serving as a hook for automated tests.
   */
  testId?: string;
}

export interface SkeletonHeadingItemProps {
  /**
   * Width of the skeleton heading item.
   * Generally you don't need to specify this as it has a staggered width based on `:nth-child` by default.
   */
  width?: Width;

  /**
   * A `testId` prop is provided for specified elements,
   * which is a unique string that appears as a data attribute `data-testid` in the rendered code,
   * serving as a hook for automated tests.
   */
  testId?: string;

  /**
   * Causes to the skeleton to have a slight horizontal shimmer.
   * Only use this when you want to bring more attention to the loading content.
   */
  isShimmering?: boolean;
}

export type ItemState = { isSelected: boolean; isDisabled: boolean };

/**
 * A function that can be used to override the styles of
 * menu components. It receives the current styles and state
 * and expects a styles object.
 */
export type CSSFn = (
  currentStyles: CSSObject,
  currentState: ItemState,
) => CSSObject;

export type StatelessCssFn = (currentStyles: CSSObject) => CSSObject;

import React from 'react';
import AsyncLayoutManagerWithViewController from '../../index';
import ItemsRenderer, { TypedItemsRenderer } from '../../../../../renderer';

const noop = () => null;

class CustomItemsRenderer extends TypedItemsRenderer {}

<AsyncLayoutManagerWithViewController
  globalNavigation={noop}
  containerSkeleton={noop}
  itemsRenderer={noop}
>
  Page
</AsyncLayoutManagerWithViewController>;

<AsyncLayoutManagerWithViewController
  globalNavigation={noop}
  containerSkeleton={noop}
  itemsRenderer={ItemsRenderer}
>
  Page
</AsyncLayoutManagerWithViewController>;

<AsyncLayoutManagerWithViewController
  globalNavigation={noop}
  containerSkeleton={noop}
  itemsRenderer={CustomItemsRenderer}
>
  Page
</AsyncLayoutManagerWithViewController>;

<AsyncLayoutManagerWithViewController
  globalNavigation={noop}
  containerSkeleton={noop}
  itemsRenderer={noop}
/>;

<AsyncLayoutManagerWithViewController>
  Page
</AsyncLayoutManagerWithViewController>;

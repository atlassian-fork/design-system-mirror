import React from 'react';
import SortableGroup from '../../index';

<SortableGroup id="my-group">Foo</SortableGroup>;
<SortableGroup>Foo</SortableGroup>;
<SortableGroup id="my-group" />;

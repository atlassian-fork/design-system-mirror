import React from 'react';
import SortableItem from '../../index';

<SortableItem index={0} id="my-item" />;

<SortableItem index={0} />;
<SortableItem id="my-item" />;
<SortableItem id={5} />;

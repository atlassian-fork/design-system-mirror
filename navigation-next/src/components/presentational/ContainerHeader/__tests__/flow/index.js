import React from 'react';
import ContainerHeader from '../../index';

<ContainerHeader />;
<ContainerHeader text="My header" />;
<ContainerHeader styles={styles => styles} />;

<ContainerHeader id={5} />;
<ContainerHeader styles={5} />;

import React from 'react';
import SearchIcon from '@atlaskit/icon/glyph/search';
import GlobalNav from '../../index';

<GlobalNav primaryItems={[]} secondaryItems={[]} />;
<GlobalNav
  itemComponent={() => null}
  primaryItems={[{ id: 'foo', icon: SearchIcon, onClick: () => {} }]}
  secondaryItems={[]}
/>;

<GlobalNav primaryItems={[{ id: 'foo', onClick: 5 }]} secondaryItems={[]} />;
<GlobalNav itemComponent="foo" primaryItems={[]} secondaryItems={[]} />;
<GlobalNav />;

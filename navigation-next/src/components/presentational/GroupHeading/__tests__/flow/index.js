import React from 'react';
import GroupHeading from '../../index';

<GroupHeading>Foo</GroupHeading>;
<GroupHeading after={() => 'Bar'}>Foo</GroupHeading>;

<GroupHeading />;

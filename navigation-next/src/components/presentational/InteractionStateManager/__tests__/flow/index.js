import React from 'react';
import InteractionStateManager from '../../index';

<InteractionStateManager>
  {({ isActive, isHover, isFocused }) => null}
</InteractionStateManager>;

<InteractionStateManager />;
<InteractionStateManager>Foo</InteractionStateManager>;

import React from 'react';

import Item, { ItemBase } from '../../index';
import { create } from 'domain';

const hocProvidedProps = {
  createAnalyticsEvent: () => null,
};

/** ItemBase */
<ItemBase {...hocProvidedProps} />;
<ItemBase {...hocProvidedProps} component={() => null} />;

<ItemBase id={5} />;
<ItemBase component={null} />;

/** Item */
<Item />;
<Item component={() => null} />;

<Item id={5} />;
<Item component={null} />;

import React from 'react';
import ItemAvatar from '../../index';

<ItemAvatar
  itemState={{
    isActive: false,
    isHover: false,
    isFocused: false,
    isSelected: false,
    spacing: 'default',
  }}
/>;

<ItemAvatar />;
<ItemAvatar itemState={{}} />;

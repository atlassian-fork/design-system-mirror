import React from 'react';
import Section from '../../index';

<Section>
  {({ className }) => <div className={className}>My section</div>}
</Section>;
<Section id="section" shouldGrow alwaysShowScrollHint>
  {({ className }) => <div className={className}>My section</div>}
</Section>;

<Section />;
<Section>My children</Section>;
<Section id={5}>
  {({ className }) => <div className={className}>My section</div>}
</Section>;

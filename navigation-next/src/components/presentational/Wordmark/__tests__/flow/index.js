import React from 'react';
import { AtlassianWordmark } from '@atlaskit/logo';
import Wordmark from '../../index';

<Wordmark wordmark={AtlassianWordmark} />;

<Wordmark wordmark={<div />} />;
<Wordmark foo="bar" />;

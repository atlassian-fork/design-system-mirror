import React, { Component } from 'react';
import withTheme, { withContentTheme, withGlobalTheme } from '../../withTheme';

class Bar extends Component {
  render() {
    return null;
  }
}

<Bar theme="test" />;

/**
 * BarWithTheme
 */

const BarWithTheme = withTheme()(Bar);

<BarWithTheme />;

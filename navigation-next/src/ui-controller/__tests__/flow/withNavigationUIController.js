import React, { Component } from 'react';
import UIController from '../../UIController';
import withNavigationUIController from '../../withNavigationUIController';

class Bar extends Component {
  render() {
    return null;
  }
}

<Bar navigationUIController="test" />;

/**
 * BarWithNavigationUIController
 */

const BarWithNavigationUIController = withNavigationUIController(Bar);

<BarWithNavigationUIController />;

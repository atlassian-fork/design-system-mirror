import React from 'react';
import SectionMessage from '../src';

const Example = () => (
  <SectionMessage title="Look elsewhere" appearance="warning">
    <p>
      The web site you seek
      <br />
      cannot be located. But
      <br />
      endless others exist
    </p>
  </SectionMessage>
);

export default Example;
